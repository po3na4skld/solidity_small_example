// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "./FanToken.sol";
import "../interfaces/IERC20.sol";

contract TokenSale {
    FanToken private _token;
    IERC20 private _usdt_contract;
    address payable private _owner;

    uint256 public total_on_sale;
    uint256 public sale_price;
    uint256 public distributed;

    mapping(address => uint256) private locked_balances;
    mapping(address => uint) private lock_ends;
    mapping(address => uint256) private sold_balances;

    uint public lock_duration;
    uint public end_ts;
    uint256 public lock_amount_pct;

    uint256 public min_amount;
    uint256 public max_amount;

    event Distributed(address indexed to, uint256 locked_amount, uint256 distributed_amount);
    event Withdrawal(address indexed to, uint256 amount);

    constructor(
        address token_address,
        address usdt_address,
        uint256 total_on_sale_,
        uint256 sale_price_,
        uint lock_duration_,
        uint lock_amount_,
        uint256 min_amount_,
        uint256 max_amount_,
        uint duration
    ) {
        _token = FanToken(token_address);
        _usdt_contract = IERC20(usdt_address);
        _owner = payable(msg.sender);
        total_on_sale = total_on_sale_;
        sale_price = sale_price_;
        lock_duration = lock_duration_;
        lock_amount_pct = lock_amount_;
        min_amount = min_amount_;
        max_amount = max_amount_;
        end_ts = block.timestamp + duration;
    }

    function amountToTokens(uint256 amount) public view returns (uint256) {
        return amount / sale_price * 100;
    }

    function allowedToBuy(address buyer) external view returns (bool) {
        return sold_balances[buyer] < max_amount;
    }

    function checkLock(address to) public view returns (bool){
        return block.timestamp >= lock_ends[to];
    }

    function checkDistributionAllowed(address to, uint256 amount) internal {
        require(block.timestamp < end_ts, "Token sale ended");
        require(sold_balances[to] + amount <= max_amount, "Amount is greater than user allowed to buy");
        require(distributed + amount <= total_on_sale, "Amount is greater than tokens left on sale");
        require(address(0) != to, "Zero address");
        require(locked_balances[to] + amount <= max_amount, "Amount is reaching the sale limit");

        if(locked_balances[to] == 0 && amount < min_amount) {
            revert("Amount is less than minimum allowed sale size");
        }
    }

    function distribute(address to, uint256 amount) public returns (bool) {
        uint256 tokens_to_distribute = amountToTokens(amount);
        checkDistributionAllowed(to, tokens_to_distribute);

        bool approved = _usdt_contract.approve(to, amount);
        if(approved) {
            _usdt_contract.transferFrom(to, _owner, amount);

            uint256 tokens_to_lock = tokens_to_distribute / lock_amount_pct;
            uint256 tokens_to_withdraw = tokens_to_distribute - tokens_to_lock;

            locked_balances[to] = tokens_to_lock;
            lock_ends[to] = block.timestamp + lock_duration;
            _token.mint(to, tokens_to_withdraw);

            distributed += tokens_to_distribute;

            emit Distributed(to, tokens_to_lock, tokens_to_withdraw);
            return true;
        }

        return false;
    }

    function withdraw(address to) public returns (bool){
        require(checkLock(to), "Too early for withdraw");
        _token.mint(to, locked_balances[to]);
        emit Withdrawal(to, locked_balances[to]);
        locked_balances[to] = 0;

        return true;
    }

}
